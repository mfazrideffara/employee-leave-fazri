package com.employeeleave.employeeleave.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.employeeleave.employeeleave.model.BucketApproval;
import com.employeeleave.employeeleave.repository.ListRequestLeaveRepository;

@Service
public class EmployeeLeaveService 
{
    @Autowired
    ListRequestLeaveRepository listRequestLeaveRepository;
     
    public List<BucketApproval> getAllLeaveRequest(Integer pageNo, Integer pageSize)
    {
    	Pageable paging = PageRequest.of(pageNo, pageSize);
    	 
    	Page<BucketApproval> pagedResult = listRequestLeaveRepository.findAll(paging); 
         
        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<BucketApproval>();
        }
    }
}
