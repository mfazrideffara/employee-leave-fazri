package com.employeeleave.employeeleave.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employeeleave.employeeleave.model.PositionLeave;

@Repository
public interface PositionLeaveRepository extends JpaRepository<PositionLeave, Integer>{

}
