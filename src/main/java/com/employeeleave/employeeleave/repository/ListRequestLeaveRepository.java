package com.employeeleave.employeeleave.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.employeeleave.employeeleave.model.BucketApproval;

@Repository
public interface ListRequestLeaveRepository extends PagingAndSortingRepository<BucketApproval, Integer>{

}
