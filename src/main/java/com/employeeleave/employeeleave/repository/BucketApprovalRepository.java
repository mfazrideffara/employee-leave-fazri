package com.employeeleave.employeeleave.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employeeleave.employeeleave.model.BucketApproval;

@Repository
public interface BucketApprovalRepository extends JpaRepository<BucketApproval, Integer> {

}
