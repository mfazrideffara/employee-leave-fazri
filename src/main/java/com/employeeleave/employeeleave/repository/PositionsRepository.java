package com.employeeleave.employeeleave.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employeeleave.employeeleave.model.Positions;

@Repository
public interface PositionsRepository extends JpaRepository<Positions, Integer>{

}
