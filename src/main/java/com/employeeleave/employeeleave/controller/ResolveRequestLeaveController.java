package com.employeeleave.employeeleave.controller;

import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employeeleave.employeeleave.dto.BucketApprovalDTO;
import com.employeeleave.employeeleave.model.BucketApproval;
import com.employeeleave.employeeleave.repository.BucketApprovalRepository;

@RestController
@RequestMapping("/api")
public class ResolveRequestLeaveController {
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	BucketApprovalRepository bucketApprovalRepository;
	
	public BucketApprovalDTO convertToDTO(BucketApproval BucketApproval) {
		return modelMapper.map(BucketApproval, BucketApprovalDTO.class);
	}

	public BucketApproval convertToEntity(BucketApprovalDTO bucketApprovalDTO) {
		return modelMapper.map(bucketApprovalDTO, BucketApproval.class);
	}
	
	// Create a new RequestLeave
	@PostMapping("/resolveRequest")
	public HashMap<String, Object> createRequestLeave(@Valid @RequestBody BucketApprovalDTO bucketApprovalDTO) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	BucketApproval bucketApproval = convertToEntity(bucketApprovalDTO);
    	String message = "";
    	int remainingDaysOff = getRemainingDaysOff(bucketApproval);
		int leaveDays = getLeaveDays(bucketApproval);
    	
    	if(!isExist(bucketApproval)) {
    		message = "Permohonan dengan ID "+bucketApproval.getBucketApprovalId()+" tidak ditemukan." ;
    	} else if (isInvalid(bucketApproval)) {
    		message = "Kesalahan data, tanggal keputusan tidak bisa lebih awal dari pengajuan cuti.";
    	} else {
    		for(BucketApproval ba : bucketApprovalRepository.findAll()) {
    			if(ba.getBucketApprovalId() == bucketApproval.getBucketApprovalId()) {
    				ba.setResolverReason(bucketApproval.getResolverReason());
    				ba.getUserLeaveRequest().setStatus((bucketApproval.getUserLeaveRequest().getStatus()));
    				if(bucketApproval.getUserLeaveRequest().getStatus().equalsIgnoreCase("approved")) {
    		    		ba.getUserLeaveRequest().setRemainingDaysOff(remainingDaysOff - Math.abs(leaveDays));
    		    	}
    				ba.setResolvedBy(bucketApproval.getResolvedBy());
    				ba.setResolvedDate(bucketApproval.getResolvedDate());
    				bucketApprovalRepository.save(ba);
    				message = "Permohonan dengan ID "+bucketApproval.getBucketApprovalId()+" telah berhasil diputuskan.";
    			}
    		}
    	}
    	showHashMap.put("Message", message);
    	showHashMap.put("Data", bucketApprovalDTO);
    	
    	return showHashMap;
    }
	
	// Proses mengambil sisa cuti
	public int getRemainingDaysOff(BucketApproval bucketApproval) {
		int remainingDaysOff = 0;
		for(BucketApproval ba : bucketApprovalRepository.findAll()) {
			if(ba.getBucketApprovalId() == bucketApproval.getBucketApprovalId()) {
				remainingDaysOff = ba.getUserLeaveRequest().getRemainingDaysOff();
			}
		}
		return remainingDaysOff;
	}
	
	// Proses mengambil banyak jumlah hari cuti yang diajukan
	public int getLeaveDays(BucketApproval bucketApproval) {
		Long leaveDays = new Long(0);
		for(BucketApproval ba : bucketApprovalRepository.findAll()) {
			if(ba.getBucketApprovalId() == bucketApproval.getBucketApprovalId()) {
				leaveDays = (ba.getUserLeaveRequest().getLeaveDateTo().getTime() - ba.getUserLeaveRequest().getLeaveDateFrom().getTime()) / 1000 / 60 / 60 / 24;
			}
		}
		return leaveDays.intValue();
	}
	
	// Proses validasi id bucket approval
	public boolean isExist(BucketApproval bucketApproval) {
		boolean isExist = false;
		for(BucketApproval ba : bucketApprovalRepository.findAll()) {
			if(ba.getBucketApprovalId() == bucketApproval.getBucketApprovalId()) {
				isExist = true;
			}
		}
		return isExist;
	}
	
	// Proses validasi tanggal resolve
	public boolean isInvalid(BucketApproval bucketApproval) {
		boolean isInvalid = false;
		for(BucketApproval ba : bucketApprovalRepository.findAll()) {
			if(ba.getBucketApprovalId() == bucketApproval.getBucketApprovalId()) {
				if(bucketApproval.getResolvedDate().compareTo(ba.getUserLeaveRequest().getLeaveDateFrom()) < 0) {
					isInvalid = true;
				}
			}
		}
		return isInvalid;
	}
}
