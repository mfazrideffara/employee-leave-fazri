package com.employeeleave.employeeleave.controller;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employeeleave.employeeleave.dto.UserLeaveRequestDTO;
import com.employeeleave.employeeleave.model.BucketApproval;
import com.employeeleave.employeeleave.model.PositionLeave;
import com.employeeleave.employeeleave.model.UserLeaveRequest;
import com.employeeleave.employeeleave.model.Users;
import com.employeeleave.employeeleave.repository.BucketApprovalRepository;
import com.employeeleave.employeeleave.repository.PositionLeaveRepository;
import com.employeeleave.employeeleave.repository.PositionsRepository;
import com.employeeleave.employeeleave.repository.UserLeaveRequestRepository;
import com.employeeleave.employeeleave.repository.UsersRepository;


@RestController
@RequestMapping("/api")
public class RequestLeaveController {
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	UserLeaveRequestRepository userLeaveRequestRepository;
	
	@Autowired
	PositionsRepository positionsRepository;
	
	@Autowired
	PositionLeaveRepository positionLeaveRepository;
	
	@Autowired
	BucketApprovalRepository bucketApprovalRepository;
	
	@Autowired
	UsersRepository usersRepository;
	
	public UserLeaveRequestDTO convertToDTO(UserLeaveRequest userLeaveRequest) {
		return modelMapper.map(userLeaveRequest, UserLeaveRequestDTO.class);
	}

	public UserLeaveRequest convertToEntity(UserLeaveRequestDTO userLeaveRequestDTO) {
		return modelMapper.map(userLeaveRequestDTO, UserLeaveRequest.class);
	}
	
	// Create a new RequestLeave
	@PostMapping("/requestLeave")
	public HashMap<String, Object> createRequestLeave(@Valid @RequestBody UserLeaveRequestDTO userLeaveRequestDTO) throws ParseException {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	UserLeaveRequest userLeaveRequest = convertToEntity(userLeaveRequestDTO);
    	ZoneId defaultZoneId = ZoneId.systemDefault();
    	LocalDate localDate = LocalDate.now();
    	Date now = Date.from(localDate.atStartOfDay(defaultZoneId).toInstant());
    	userLeaveRequest.setLeaveRequestDate(now);
    	String message = "";
    	int allotedLeave = getAllotedLeave(userLeaveRequest);
    	int leaveDays = getDaysBetween(userLeaveRequest.getLeaveDateFrom(), userLeaveRequest.getLeaveDateTo());
    	
    	if(isLeaveRunningOut(allotedLeave)) {
    		message = "Mohon maaf, jatah cuti Anda telah habis.";
    	} else if (isInadequateLeave(allotedLeave, leaveDays)) {
    		message = "Mohon maaf, jatah cuti Anda tidak cukup untuk digunakan dari tanggal "+ userLeaveRequest.getLeaveDateFrom() +" sampai "+userLeaveRequest.getLeaveDateTo()+" ("+leaveDays+" hari). Jatah cuti Anda yang tersisa adalah "+allotedLeave+" hari.";
    	} else if (isInvalidDate(userLeaveRequest.getLeaveDateFrom(), userLeaveRequest.getLeaveDateTo())) {
    		message = "Tanggal yang Anda ajukan tidak valid.";
    	} else if (isBlackDate(userLeaveRequest.getLeaveDateFrom(), userLeaveRequest.getLeaveRequestDate())){
    		message = "Tanggal yang Anda ajukan telah lampau, silahkan ganti tanggal pengajuan cuti anda.";
    	} else {
    		message = "Permohonan Anda sedang diproses."+leaveDays;
    		userLeaveRequest.setStatus("Waiting");
    		userLeaveRequest.setRemainingDaysOff(allotedLeave);
    		userLeaveRequestRepository.save(userLeaveRequest);
    		BucketApproval bucketApproval = new BucketApproval();
    		bucketApproval.setUserLeaveRequest(userLeaveRequest);
    		bucketApprovalRepository.save(bucketApproval);
    	}
    
    	showHashMap.put("Message", message);
    	showHashMap.put("Data", userLeaveRequest);
    	
    	return showHashMap;
    }
	
	// Proses Mencari jatah cuti
	public int getAllotedLeave(UserLeaveRequest userLeaveRequest) {
		int allotedLeave = 0;	
		List<UserLeaveRequest> resultList = userLeaveRequestRepository.findAll();
		
		if(resultList.isEmpty()) {
			for(Users u : usersRepository.findAll()) {
				if(u.getUserId() == userLeaveRequest.getUsers().getUserId()) {
					for(PositionLeave p : positionLeaveRepository.findAll()) {
						if(p.getPositions().getPositionId() == u.getPositions().getPositionId()) {
							allotedLeave = p.getMaxLeaveDays();
						}
					}
				}
			}
		} else {
			for(UserLeaveRequest ul : resultList) {
				if(ul.getUsers().getUserId() == userLeaveRequest.getUsers().getUserId()) {
					allotedLeave = ul.getRemainingDaysOff();
				} else {
					for(Users u : usersRepository.findAll()) {
						if(u.getUserId() == userLeaveRequest.getUsers().getUserId()) {
							for(PositionLeave p : positionLeaveRepository.findAll()) {
								if(p.getPositions().getPositionId() == u.getPositions().getPositionId()) {
									allotedLeave = p.getMaxLeaveDays();
								}
							}
						}
					}
				}
			}
		}
		
		return allotedLeave;
	}
	
	// Proses Mencari jumlah hari cuti yang diajukan
	public int getDaysBetween(Date leaveDateFrom, Date leaveDateTo) {
		Long leaveDays = new Long(0);
		leaveDays = (leaveDateTo.getTime() - leaveDateFrom.getTime()) / 1000 / 60 / 60 / 24;
		return leaveDays.intValue();
	}
	 
	// Proses validasi jatah cuti telah habis
	public boolean isLeaveRunningOut(int allotedLeave) {
		boolean isRunningOut = false;
		if(allotedLeave == 0) {
			isRunningOut = true;
		}
		
		return isRunningOut;
	}
	
	// Proses validasi jatah cuti mencukupi
	public boolean isInadequateLeave(int allotedLeave, int daysBetween) {
		boolean isInadequate = false;
		if(daysBetween > allotedLeave) {
			isInadequate = true;
		}
		return isInadequate;
	}
	
	// Proses validasi tanggal salah (tanggal mulai cuti lebih besar dari tanggal akhir cuti)
	public boolean isInvalidDate(Date leaveDateFrom, Date leaveDateTo) {
		boolean isInvalidDate = false;
		if(leaveDateFrom.compareTo(leaveDateTo) > 0) {
			isInvalidDate = true;
		}
		return isInvalidDate;
	}
	
	// Proses validasi tanggal salah (tanggal cuti diajukan sebelum hari ini)
	public boolean isBlackDate(Date leaveDateFrom, Date leaveRequestDate) {
		boolean isBlackDate = false;
		if(leaveDateFrom.compareTo(leaveRequestDate) < 0) {
			isBlackDate = true;
		}
		return isBlackDate;
	}
}
