package com.employeeleave.employeeleave.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employeeleave.employeeleave.dto.PositionLeaveDTO;
import com.employeeleave.employeeleave.model.PositionLeave;
import com.employeeleave.employeeleave.repository.PositionLeaveRepository;

@RestController
@RequestMapping("/api")
public class PositionLeaveController {

	ModelMapper modelMapper = new ModelMapper();

	@Autowired
	PositionLeaveRepository positionLeaveRepository;
	
	public PositionLeaveDTO convertToDTO(PositionLeave positionLeave) {
		return modelMapper.map(positionLeave, PositionLeaveDTO.class);
	}

	public PositionLeave convertToEntity(PositionLeaveDTO positionLeaveDTO) {
		return modelMapper.map(positionLeaveDTO, PositionLeave.class);
	}

	// Get All PositionLeave
	@GetMapping("/positionLeave/all")
	public HashMap<String, Object> getAllPositionLeave() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<PositionLeaveDTO> listPositionLeaves = new ArrayList<PositionLeaveDTO>();
		for (PositionLeave p : positionLeaveRepository.findAll()) {
			PositionLeaveDTO positionLeaveDTO = convertToDTO(p);
			listPositionLeaves.add(positionLeaveDTO);
		}

		String message;
		if (listPositionLeaves.isEmpty()) {
			message = "Read All Failed!";
		} else {
			message = "Read All Success!";
		}
		showHashMap.put("Message", message);
		showHashMap.put("Total", listPositionLeaves.size());
		showHashMap.put("Data", listPositionLeaves);

		return showHashMap;
	}

	// Read PositionLeave By ID
	@GetMapping("/positionLeave/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Integer id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		PositionLeave positionLeave = positionLeaveRepository.findById(id).orElse(null);
		PositionLeaveDTO positionLeaveDTO = convertToDTO(positionLeave);
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", positionLeaveDTO);
		return showHashMap;
	}

	// Create a new PositionLeave
		@PostMapping("/positionLeave/add")
		public HashMap<String, Object> createPositionLeave(@Valid @RequestBody ArrayList<PositionLeaveDTO> positionLeaveDTO) {
	    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
	    	@Valid ArrayList<PositionLeaveDTO> listPositionLeaves = positionLeaveDTO;
	    	String message;
	    	
	    	for(PositionLeaveDTO p : listPositionLeaves) {
	    		PositionLeave positionLeave = convertToEntity(p);
	    		positionLeaveRepository.save(positionLeave);
	    	}
	    
	    	if(listPositionLeaves == null) {
	    		message = "Create Failed!";
	    	} else {
	    		message = "Create Success!";
	    	}
	    	
	    	showHashMap.put("Message", message);
	    	showHashMap.put("Total Insert", listPositionLeaves.size());
	    	showHashMap.put("Data", listPositionLeaves);
	    	
	    	return showHashMap;
	    }

	// Update a PositionLeave
	@PutMapping("/positionLeave/update/{id}")
	public HashMap<String, Object> updatePositionLeave(@PathVariable(value = "id") Integer id,
			@Valid @RequestBody PositionLeaveDTO positionLeaveDetails) {

		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;

		PositionLeave positionLeave = positionLeaveRepository.findById(id).orElse(null);
		
		positionLeaveDetails.setPositionLeaveId(positionLeave.getPositionLeaveId());
		
		if (positionLeaveDetails.getPositions() != null) {
			positionLeave.setPositions(convertToEntity(positionLeaveDetails).getPositions());
		}
		if (positionLeaveDetails.getMaxLeaveDays() != 0) {
			positionLeave.setMaxLeaveDays(convertToEntity(positionLeaveDetails).getMaxLeaveDays());
		}

		PositionLeave updatePositionLeave = positionLeaveRepository.save(positionLeave);

		List<PositionLeave> resultList = new ArrayList<PositionLeave>();
		resultList.add(updatePositionLeave);

		if (resultList.isEmpty()) {
			message = "Update Failed!";
		} else {
			message = "Update Success!";
		}

		showHashMap.put("Message", message);
		showHashMap.put("Total Update", resultList.size());
		showHashMap.put("Data", resultList);

		return showHashMap;
	}

	// Delete a PositionLeave
	@DeleteMapping("/PositionLeave/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Integer id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		PositionLeave positionLeave = positionLeaveRepository.findById(id).orElse(null);

		PositionLeaveDTO positionLeaveDTO = convertToDTO(positionLeave);
		positionLeaveRepository.delete(positionLeave);

		showHashMap.put("Messages", "Delete Data Success!");
		showHashMap.put("Delete data :", positionLeaveDTO);
		return showHashMap;
	}
}
