package com.employeeleave.employeeleave.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employeeleave.employeeleave.dto.UsersDTO;
import com.employeeleave.employeeleave.model.Users;
import com.employeeleave.employeeleave.repository.UsersRepository;

@RestController
@RequestMapping("/api")
public class UsersController {

	ModelMapper modelMapper = new ModelMapper();

	@Autowired
	UsersRepository usersRepository;

	public UsersDTO convertToDTO(List<Users> listUserss) {
		UsersDTO usersDTO = modelMapper.map(listUserss, UsersDTO.class);
		return usersDTO;
	}

	public UsersDTO convertToDTO(Users users) {
		return modelMapper.map(users, UsersDTO.class);
	}

	public Users convertToEntity(UsersDTO usersDTO) {
		return modelMapper.map(usersDTO, Users.class);
	}

	// Get All Users
	@GetMapping("/users/all")
	public HashMap<String, Object> getAllUsers() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<UsersDTO> listUserss = new ArrayList<UsersDTO>();
		for (Users u : usersRepository.findAll()) {
			UsersDTO usersDTO = convertToDTO(u);
			listUserss.add(usersDTO);
		}

		String message;
		if (listUserss.isEmpty()) {
			message = "Read All Failed!";
		} else {
			message = "Read All Success!";
		}
		showHashMap.put("Message", message);
		showHashMap.put("Total", listUserss.size());
		showHashMap.put("Data", listUserss);

		return showHashMap;
	}

	// Read Users By ID
	@GetMapping("/users/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Integer id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Users users = usersRepository.findById(id).orElse(null);
		UsersDTO usersDTO = convertToDTO(users);
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", usersDTO);
		return showHashMap;
	}

	// Method Validasi No KTP
	public boolean noKtpValidation(UsersDTO usersDTO) {
		boolean isDuplicated = false;
		for (Users k : usersRepository.findAll()) {
			if (k.getNoKtp().equalsIgnoreCase(usersDTO.getNoKtp())) {
				isDuplicated = true;
			}
		}
		return isDuplicated;
	}

	// Create a new Users
	@PostMapping("/users/add")
	public HashMap<String, Object> createUsers(@Valid @RequestBody ArrayList<UsersDTO> usersDTO) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		ArrayList<UsersDTO> listUserss = usersDTO;
		ArrayList<Users> resultSuccess = new ArrayList<Users>();
		ArrayList<Users> resultError = new ArrayList<Users>();
		String message = "";
		String errorMessage = "";
		boolean isDuplicated = false;
		for (UsersDTO u : listUserss) {
			Users users = convertToEntity(u);
			isDuplicated = noKtpValidation(u);
			if (!isDuplicated) {
				usersRepository.save(users);
				resultSuccess.add(users);
			} else {
				resultError.add(users);
				errorMessage = "Nomor KTP is already in Database!";
			}

		}

		if (listUserss.isEmpty() || resultSuccess.isEmpty()) {
			message = "Create Failed!";
		} else {
			message = "Create Success!";
		}

		showHashMap.put("Message", message);
		showHashMap.put("Error Message", errorMessage);
		showHashMap.put("Total Insert Success", resultSuccess.size());
		showHashMap.put("Total Insert Failed", resultError.size());
		showHashMap.put("Data Success", resultSuccess);
		showHashMap.put("Data Failed", resultError);

		return showHashMap;
	}

	// Update a Users
	@PutMapping("/users/update/{id}")
	public HashMap<String, Object> updateUsers(@PathVariable(value = "id") Integer id,
			@Valid @RequestBody UsersDTO usersDetails) {

		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;

		Users users = usersRepository.findById(id).orElse(null);
		
		usersDetails.setUserId(users.getUserId());
		
		if (usersDetails.getPositions() != null) {
			users.setPositions(convertToEntity(usersDetails).getPositions());
		}
		if (usersDetails.getName() != null) {
			users.setName(convertToEntity(usersDetails).getName());
		}
		if (usersDetails.getNoKtp() != null) {
			users.setNoKtp(convertToEntity(usersDetails).getNoKtp());
		}
		if(usersDetails.getAddress() != null) {
			users.setAddress(convertToEntity(usersDetails).getAddress());
		}
		if(usersDetails.getDateOfBirth() != null) {
			users.setDateOfBirth(convertToEntity(usersDetails).getDateOfBirth());
		}
		if(usersDetails.getGender() != null) {
			users.setGender(convertToEntity(usersDetails).getGender());
		}
		if(usersDetails.getMaritalStatus() != null) {
			users.setMaritalStatus(convertToEntity(usersDetails).getMaritalStatus());
		}
		
		Users updateUsers = usersRepository.save(users);

		List<Users> resultList = new ArrayList<Users>();
		resultList.add(updateUsers);

		if (resultList.isEmpty()) {
			message = "Update Failed!";
		} else {
			message = "Update Success!";
		}

		showHashMap.put("Message", message);
		showHashMap.put("Total Update", resultList.size());
		showHashMap.put("Data", resultList);

		return showHashMap;
	}

	// Delete a Users
	@DeleteMapping("/users/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Integer id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Users users = usersRepository.findById(id).orElse(null);

		UsersDTO usersDTO = convertToDTO(users);
		usersRepository.delete(users);

		showHashMap.put("Messages", "Delete Data Success!");
		showHashMap.put("Delete data :", usersDTO);
		return showHashMap;
	}
}
