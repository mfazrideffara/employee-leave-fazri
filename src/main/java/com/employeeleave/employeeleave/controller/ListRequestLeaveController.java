package com.employeeleave.employeeleave.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.employeeleave.employeeleave.dto.BucketApprovalDTO;
import com.employeeleave.employeeleave.model.BucketApproval;
import com.employeeleave.employeeleave.repository.BucketApprovalRepository;
import com.employeeleave.employeeleave.service.EmployeeLeaveService;

@RestController
@RequestMapping("/api")
public class ListRequestLeaveController {
	
	ModelMapper modelMapper = new ModelMapper();

	@Autowired
	BucketApprovalRepository bucketApprovalRepository;
	
	@Autowired
	EmployeeLeaveService employeeLeaveService;
	
	public BucketApprovalDTO convertToDTO(BucketApproval bucketApproval) {
		return modelMapper.map(bucketApproval, BucketApprovalDTO.class);
	}

	
	// Get All PositionLeave
	@GetMapping("/listRequestLeave")
	public HashMap<String, Object> getResolveRequest(@RequestParam(defaultValue= "0") Integer userId,
													 @RequestParam(defaultValue = "10") Integer totalDataPerPage,
													 @RequestParam(defaultValue = "0") Integer choosenPage) {
		
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<BucketApprovalDTO> resultList = new ArrayList<BucketApprovalDTO>();
	
        List<BucketApproval> listLeaveRequest = employeeLeaveService.getAllLeaveRequest(choosenPage, totalDataPerPage);
        
    	for (BucketApproval ba : listLeaveRequest) {
			BucketApprovalDTO bucketApprovalDTO = convertToDTO(ba);
			if(ba.getUserLeaveRequest().getUsers().getUserId() == userId) {
				resultList.add(bucketApprovalDTO);
			} else if (userId == 0) {
				resultList.add(bucketApprovalDTO);
			}
		}
		String message;
		if (resultList.isEmpty()) {
			message = "Read All Success! ( No Data )";
		} else {
			message = "Read All Success!";
		}
		showHashMap.put("Message", message);
		showHashMap.put("Total", resultList.size());
		showHashMap.put("Data", resultList);

		return showHashMap;
	}
}
