package com.employeeleave.employeeleave.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.employeeleave.employeeleave.dto.BucketApprovalDTO;
import com.employeeleave.employeeleave.model.BucketApproval;
import com.employeeleave.employeeleave.repository.BucketApprovalRepository;

@RestController
@RequestMapping("/api")
public class StatusLeaveController {
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	BucketApprovalRepository bucketApprovalRepository;
	
	public BucketApprovalDTO convertToDTO(BucketApproval bucketApproval) {
		return modelMapper.map(bucketApproval, BucketApprovalDTO.class);
	}

	public BucketApproval convertToEntity(BucketApprovalDTO bucketApprovalDTO) {
		return modelMapper.map(bucketApprovalDTO, BucketApproval.class);
	}
	
	// Get All Request Leave By Status
	@GetMapping("/statusLeave")
	public HashMap<String, Object> getLeaveRequestByStatus(@RequestParam(value="status") String status) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<BucketApprovalDTO> listBucketApproval = new ArrayList<BucketApprovalDTO>();
		String message;
		for (BucketApproval ba : bucketApprovalRepository.findAll()) {
			BucketApprovalDTO bucketApprovalDTO = convertToDTO(ba);
			if(ba.getUserLeaveRequest().getStatus().equalsIgnoreCase(status)){
				listBucketApproval.add(bucketApprovalDTO);
			}
		}
		if (listBucketApproval.isEmpty()) {
			message = "Read All Success! ( No Data )";
		} else {
			message = "Read All Success!";
		}
		showHashMap.put("Message", message);
		showHashMap.put("Total", listBucketApproval.size());
		showHashMap.put("Data", listBucketApproval);
		
		return showHashMap;
	}
}
