package com.employeeleave.employeeleave.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employeeleave.employeeleave.dto.PositionsDTO;
import com.employeeleave.employeeleave.model.Positions;
import com.employeeleave.employeeleave.repository.PositionsRepository;

@RestController
@RequestMapping("/api")
public class PositionsController {
	
	ModelMapper modelMapper = new ModelMapper();

	@Autowired
	PositionsRepository positionsRepository;
	
	public PositionsDTO convertToDTO(Positions positions) {
		return modelMapper.map(positions, PositionsDTO.class);
	}

	public Positions convertToEntity(PositionsDTO positionsDTO) {
		return modelMapper.map(positionsDTO, Positions.class);
	}

	// Get All Positions
	@GetMapping("/positions/all")
	public HashMap<String, Object> getAllPositions() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<PositionsDTO> listPositionss = new ArrayList<PositionsDTO>();
		for (Positions p : positionsRepository.findAll()) {
			PositionsDTO positionsDTO = convertToDTO(p);
			listPositionss.add(positionsDTO);
		}

		String message;
		if (listPositionss.isEmpty()) {
			message = "Read All Failed!";
		} else {
			message = "Read All Success!";
		}
		showHashMap.put("Message", message);
		showHashMap.put("Total", listPositionss.size());
		showHashMap.put("Data", listPositionss);

		return showHashMap;
	}

	// Read Positions By ID
	@GetMapping("/positions/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Integer id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Positions positions = positionsRepository.findById(id).orElse(null);
		PositionsDTO positionsDTO = convertToDTO(positions);
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", positionsDTO);
		return showHashMap;
	}

	// Create a new Positions
	@PostMapping("/positions/add")
	public HashMap<String, Object> createPositions(@Valid @RequestBody ArrayList<PositionsDTO> positionsDTO) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	@Valid ArrayList<PositionsDTO> listPositionss = positionsDTO;
    	String message;
    	
    	for(PositionsDTO p : listPositionss) {
    		Positions positions = convertToEntity(p);
    		positionsRepository.save(positions);
    	}
    
    	if(listPositionss == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	showHashMap.put("Message", message);
    	showHashMap.put("Total Insert", listPositionss.size());
    	showHashMap.put("Data", listPositionss);
    	
    	return showHashMap;
    }

	// Update a Positions
	@PutMapping("/positions/update/{id}")
	public HashMap<String, Object> updatePositions(@PathVariable(value = "id") Integer id,
			@Valid @RequestBody PositionsDTO positionsDetails) {

		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;

		Positions positions = positionsRepository.findById(id).orElse(null);
		
		positionsDetails.setPositionId(positions.getPositionId());

		if (positionsDetails.getPositionName() != null) {
			positions.setPositionName(convertToEntity(positionsDetails).getPositionName());
		}

		Positions updatePositions = positionsRepository.save(positions);

		List<Positions> resultList = new ArrayList<Positions>();
		resultList.add(updatePositions);

		if (resultList.isEmpty()) {
			message = "Update Failed!";
		} else {
			message = "Update Success!";
		}

		showHashMap.put("Message", message);
		showHashMap.put("Total Update", resultList.size());
		showHashMap.put("Data", resultList);

		return showHashMap;
	}

	// Delete a Positions
	@DeleteMapping("/positions/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Integer id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Positions positions = positionsRepository.findById(id).orElse(null);

		PositionsDTO positionsDTO = convertToDTO(positions);
		positionsRepository.delete(positions);

		showHashMap.put("Messages", "Delete Data Success!");
		showHashMap.put("Delete data :", positionsDTO);
		return showHashMap;
	}
}
