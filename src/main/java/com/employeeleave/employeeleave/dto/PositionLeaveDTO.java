package com.employeeleave.employeeleave.dto;

import java.util.Date;

public class PositionLeaveDTO {
	private int positionLeaveId;
	private PositionsDTO positions;
	private int maxLeaveDays;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public PositionLeaveDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PositionLeaveDTO(int positionLeaveId, PositionsDTO positions, int maxLeaveDays, String createdBy,
			Date createdDate, String updatedBy, Date updatedDate) {
		super();
		this.positionLeaveId = positionLeaveId;
		this.positions = positions;
		this.maxLeaveDays = maxLeaveDays;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	public int getPositionLeaveId() {
		return positionLeaveId;
	}

	public PositionsDTO getPositions() {
		return positions;
	}

	public int getMaxLeaveDays() {
		return maxLeaveDays;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setPositionLeaveId(int positionLeaveId) {
		this.positionLeaveId = positionLeaveId;
	}

	public void setPositions(PositionsDTO positions) {
		this.positions = positions;
	}

	public void setMaxLeaveDays(int maxLeaveDays) {
		this.maxLeaveDays = maxLeaveDays;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
