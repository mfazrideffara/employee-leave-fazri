package com.employeeleave.employeeleave.dto;

import java.util.Date;

public class UserLeaveRequestDTO {
	private int userLeaveRequest;
	private UsersDTO users;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private String description;
	private String status;
	private Date leaveRequestDate;
	private Integer remainingDaysOff;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public UserLeaveRequestDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserLeaveRequestDTO(int userLeaveRequest, UsersDTO users, Date leaveDateFrom, Date leaveDateTo,
			String description, String status, Date leaveRequestDate, Integer remainingDaysOff, String createdBy,
			Date createdDate, String updatedBy, Date updatedDate) {
		super();
		this.userLeaveRequest = userLeaveRequest;
		this.users = users;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.description = description;
		this.status = status;
		this.leaveRequestDate = leaveRequestDate;
		this.remainingDaysOff = remainingDaysOff;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	public int getUserLeaveRequest() {
		return userLeaveRequest;
	}

	public UsersDTO getUsers() {
		return users;
	}

	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}

	public Date getLeaveDateTo() {
		return leaveDateTo;
	}

	public String getDescription() {
		return description;
	}

	public String getStatus() {
		return status;
	}

	public Date getLeaveRequestDate() {
		return leaveRequestDate;
	}

	public Integer getRemainingDaysOff() {
		return remainingDaysOff;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUserLeaveRequest(int userLeaveRequest) {
		this.userLeaveRequest = userLeaveRequest;
	}

	public void setUsers(UsersDTO users) {
		this.users = users;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setLeaveRequestDate(Date leaveRequestDate) {
		this.leaveRequestDate = leaveRequestDate;
	}

	public void setRemainingDaysOff(Integer remainingDaysOff) {
		this.remainingDaysOff = remainingDaysOff;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
