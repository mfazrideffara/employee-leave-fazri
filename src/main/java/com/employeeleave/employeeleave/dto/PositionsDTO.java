package com.employeeleave.employeeleave.dto;

import java.util.Date;

public class PositionsDTO {
	private int positionId;
	private String positionName;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public PositionsDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PositionsDTO(int positionId, String positionName, String createdBy, Date createdDate, String updatedBy,
			Date updatedDate) {
		super();
		this.positionId = positionId;
		this.positionName = positionName;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	public int getPositionId() {
		return positionId;
	}

	public String getPositionName() {
		return positionName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
