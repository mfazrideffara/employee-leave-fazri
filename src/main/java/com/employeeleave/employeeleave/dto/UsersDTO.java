package com.employeeleave.employeeleave.dto;

import java.util.Date;

public class UsersDTO {
	private int userId;
	private PositionsDTO positions;
	private String name;
	private String noKtp;
	private Date dateOfBirth;
	private String address;
	private Short maritalStatus;
	private String gender;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public UsersDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UsersDTO(int userId, PositionsDTO positions, String name, String noKtp, Date dateOfBirth, String address,
			Short maritalStatus, String gender, String createdBy, Date createdDate, String updatedBy,
			Date updatedDate) {
		super();
		this.userId = userId;
		this.positions = positions;
		this.name = name;
		this.noKtp = noKtp;
		this.dateOfBirth = dateOfBirth;
		this.address = address;
		this.maritalStatus = maritalStatus;
		this.gender = gender;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	public int getUserId() {
		return userId;
	}

	public PositionsDTO getPositions() {
		return positions;
	}

	public String getName() {
		return name;
	}

	public String getNoKtp() {
		return noKtp;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public String getAddress() {
		return address;
	}

	public Short getMaritalStatus() {
		return maritalStatus;
	}

	public String getGender() {
		return gender;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setPositions(PositionsDTO positions) {
		this.positions = positions;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setMaritalStatus(Short maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	
}
