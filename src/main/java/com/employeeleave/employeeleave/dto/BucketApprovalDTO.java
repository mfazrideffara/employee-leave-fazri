package com.employeeleave.employeeleave.dto;

import java.util.Date;

public class BucketApprovalDTO {
	private int bucketApprovalId;
	private UserLeaveRequestDTO userLeaveRequest;
	private String resolverReason;
	private String resolvedBy;
	private Date resolvedDate;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public BucketApprovalDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BucketApprovalDTO(int bucketApprovalId, UserLeaveRequestDTO userLeaveRequest, String resolverReason,
			String resolvedBy, Date resolvedDate, String createdBy, Date createdDate, String updatedBy,
			Date updatedDate) {
		super();
		this.bucketApprovalId = bucketApprovalId;
		this.userLeaveRequest = userLeaveRequest;
		this.resolverReason = resolverReason;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	public int getBucketApprovalId() {
		return bucketApprovalId;
	}

	public UserLeaveRequestDTO getUserLeaveRequest() {
		return userLeaveRequest;
	}

	public String getResolverReason() {
		return resolverReason;
	}

	public String getResolvedBy() {
		return resolvedBy;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setBucketApprovalId(int bucketApprovalId) {
		this.bucketApprovalId = bucketApprovalId;
	}

	public void setUserLeaveRequest(UserLeaveRequestDTO userLeaveRequest) {
		this.userLeaveRequest = userLeaveRequest;
	}

	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}

	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
