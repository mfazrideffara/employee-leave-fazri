/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     4/8/2020 2:35:01 PM                          */
/*==============================================================*/

/*==============================================================*/
/* Table: BUCKET_APPROVAL                                       */
/*==============================================================*/
create table BUCKET_APPROVAL (
   BUCKET_APPROVAL_ID   INT4                 not null,
   USER_LEAVE_REQUEST_ID INT4                 not null,
   RESOLVER_REASON      VARCHAR(255)         null,
   RESOLVED_BY          VARCHAR(255)         null,
   RESOLVED_DATE        DATE                 null,
   CREATED_BY           VARCHAR(255)         null,
   CREATED_DATE         DATE                 null,
   UPDATED_BY           VARCHAR(255)         null,
   UPDATED_DATE         DATE                 null,
   constraint PK_BUCKET_APPROVAL primary key (BUCKET_APPROVAL_ID)
);

comment on table BUCKET_APPROVAL is
'bucket_approval';

comment on column BUCKET_APPROVAL.BUCKET_APPROVAL_ID is
'bucket_approval_id';

comment on column BUCKET_APPROVAL.USER_LEAVE_REQUEST_ID is
'user_leave_request_id';

comment on column BUCKET_APPROVAL.RESOLVER_REASON is
'resolver_reason';

comment on column BUCKET_APPROVAL.RESOLVED_BY is
'resolved_by';

comment on column BUCKET_APPROVAL.RESOLVED_DATE is
'resolved_date';

comment on column BUCKET_APPROVAL.CREATED_BY is
'created_by';

comment on column BUCKET_APPROVAL.CREATED_DATE is
'created_date';

comment on column BUCKET_APPROVAL.UPDATED_BY is
'updated_by';

comment on column BUCKET_APPROVAL.UPDATED_DATE is
'updated_date';

/*==============================================================*/
/* Index: BUCKET_APPROVAL_PK                                    */
/*==============================================================*/
create unique index BUCKET_APPROVAL_PK on BUCKET_APPROVAL (
BUCKET_APPROVAL_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK2                                    */
/*==============================================================*/
create  index RELATIONSHIP_4_FK2 on BUCKET_APPROVAL (
USER_LEAVE_REQUEST_ID
);

/*==============================================================*/
/* Table: POSITIONS                                             */
/*==============================================================*/
create table POSITIONS (
   POSITION_ID          INT4                 not null,
   POSITION_NAME        VARCHAR(255)         not null,
   CREATED_BY           VARCHAR(255)         null,
   CREATED_DATE         DATE                 null,
   UPDATED_BY           VARCHAR(255)         null,
   UPDATED_DATE         DATE                 null,
   constraint PK_POSITIONS primary key (POSITION_ID)
);

comment on table POSITIONS is
'positions';

comment on column POSITIONS.POSITION_ID is
'position_id';

comment on column POSITIONS.POSITION_NAME is
'position_name';

comment on column POSITIONS.CREATED_BY is
'created_by';

comment on column POSITIONS.CREATED_DATE is
'created_date';

comment on column POSITIONS.UPDATED_BY is
'updated_by';

comment on column POSITIONS.UPDATED_DATE is
'updated_date';

/*==============================================================*/
/* Index: POSITION_PK                                           */
/*==============================================================*/
create unique index POSITION_PK on POSITIONS (
POSITION_ID
);

/*==============================================================*/
/* Table: POSITION_LEAVE                                        */
/*==============================================================*/
create table POSITION_LEAVE (
   POSITION_LEAVE_ID    INT4                 not null,
   POSITION_ID          INT4                 not null,
   MAX_LEAVE_DAYS       INT4                 not null,
   CREATED_BY           VARCHAR(255)         null,
   CREATED_DATE         DATE                 null,
   UPDATED_BY           VARCHAR(255)         null,
   UPDATED_DATE         DATE                 null,
   constraint PK_POSITION_LEAVE primary key (POSITION_LEAVE_ID)
);

comment on table POSITION_LEAVE is
'position_leave';

comment on column POSITION_LEAVE.POSITION_LEAVE_ID is
'position_leave_id';

comment on column POSITION_LEAVE.POSITION_ID is
'position_id';

comment on column POSITION_LEAVE.MAX_LEAVE_DAYS is
'max_leave_days';

comment on column POSITION_LEAVE.CREATED_BY is
'created_by';

comment on column POSITION_LEAVE.CREATED_DATE is
'created_date';

comment on column POSITION_LEAVE.UPDATED_BY is
'updated_by';

comment on column POSITION_LEAVE.UPDATED_DATE is
'updated_date';

/*==============================================================*/
/* Index: POSITION_LEAVE_PK                                     */
/*==============================================================*/
create unique index POSITION_LEAVE_PK on POSITION_LEAVE (
POSITION_LEAVE_ID
);

/*==============================================================*/
/* Table: USERS                                                 */
/*==============================================================*/
create table USERS (
   USER_ID              INT4                 not null,
   POSITION_ID          INT4                 not null,
   NAME                 VARCHAR(255)         not null,
   NO_KTP               VARCHAR(16)          not null,
   DATE_OF_BIRTH        DATE                 null,
   ADDRESS              TEXT                 null,
   MARITAL_STATUS       INT2                 null,
   GENDER               VARCHAR(6)           null,
   CREATED_BY           VARCHAR(255)         null,
   CREATED_DATE         DATE                 null,
   UPDATED_BY           VARCHAR(255)         null,
   UPDATED_DATE         DATE                 null,
   constraint PK_USERS primary key (USER_ID)
);

comment on table USERS is
'users';

comment on column USERS.USER_ID is
'user_id';

comment on column USERS.POSITION_ID is
'position_id';

comment on column USERS.NAME is
'name';

comment on column USERS.NO_KTP is
'no_ktp';

comment on column USERS.DATE_OF_BIRTH is
'date_of_birth';

comment on column USERS.ADDRESS is
'address';

comment on column USERS.MARITAL_STATUS is
'marital_status';

comment on column USERS.GENDER is
'gender';

comment on column USERS.CREATED_BY is
'created_by';

comment on column USERS.CREATED_DATE is
'created_date';

comment on column USERS.UPDATED_BY is
'updated_by';

comment on column USERS.UPDATED_DATE is
'updated_date';

/*==============================================================*/
/* Index: USER_PK                                               */
/*==============================================================*/
create unique index USER_PK on USERS (
USER_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_1_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_1_FK on USERS (
POSITION_ID
);

/*==============================================================*/
/* Table: USER_LEAVE_REQUEST                                    */
/*==============================================================*/
create table USER_LEAVE_REQUEST (
   USER_LEAVE_REQUEST_ID INT4                 not null,
   USER_ID              INT4                 not null,
   LEAVE_DATE_FROM      DATE                 null,
   LEAVE_DATE_TO        DATE                 null,
   DESCRIPTION          TEXT                 null,
   STATUS               VARCHAR(255)         null,
   LEAVE_REQUEST_DATE   DATE                 null,
   REMAINING_DAYS_OFF   INT4                 null,
   CREATED_BY           VARCHAR(255)         null,
   CREATED_DATE         DATE                 null,
   UPDATED_BY           VARCHAR(255)         null,
   UPDATED_DATE         DATE                 null,
   constraint PK_USER_LEAVE_REQUEST primary key (USER_LEAVE_REQUEST_ID)
);

comment on table USER_LEAVE_REQUEST is
'user_leave_request';

comment on column USER_LEAVE_REQUEST.USER_LEAVE_REQUEST_ID is
'user_leave_request_id';

comment on column USER_LEAVE_REQUEST.USER_ID is
'user_id';

comment on column USER_LEAVE_REQUEST.LEAVE_DATE_FROM is
'leave_date_from';

comment on column USER_LEAVE_REQUEST.LEAVE_DATE_TO is
'leave_date_to';

comment on column USER_LEAVE_REQUEST.DESCRIPTION is
'description';

comment on column USER_LEAVE_REQUEST.STATUS is
'status';

comment on column USER_LEAVE_REQUEST.LEAVE_REQUEST_DATE is
'leave_request_date';

comment on column USER_LEAVE_REQUEST.REMAINING_DAYS_OFF is
'remaining_days_off';

comment on column USER_LEAVE_REQUEST.CREATED_BY is
'created_by';

comment on column USER_LEAVE_REQUEST.CREATED_DATE is
'created_date';

comment on column USER_LEAVE_REQUEST.UPDATED_BY is
'updated_by';

comment on column USER_LEAVE_REQUEST.UPDATED_DATE is
'updated_date';

/*==============================================================*/
/* Index: USER_LEAVE_REQUEST_PK                                 */
/*==============================================================*/
create unique index USER_LEAVE_REQUEST_PK on USER_LEAVE_REQUEST (
USER_LEAVE_REQUEST_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_4_FK on USER_LEAVE_REQUEST (
USER_ID
);

alter table BUCKET_APPROVAL
   add constraint FK_BUCKET_A_RELATIONS_USER_LEA foreign key (USER_LEAVE_REQUEST_ID)
      references USER_LEAVE_REQUEST (USER_LEAVE_REQUEST_ID)
      on delete restrict on update restrict;

alter table POSITION_LEAVE
   add constraint FK_POSITION_RELATIONS_POSITION foreign key (POSITION_ID)
      references POSITIONS (POSITION_ID)
      on delete restrict on update restrict;

alter table USERS
   add constraint FK_USERS_RELATIONS_POSITION foreign key (POSITION_ID)
      references POSITIONS (POSITION_ID)
      on delete restrict on update restrict;

alter table USER_LEAVE_REQUEST
   add constraint FK_USER_LEA_RELATIONS_USERS foreign key (USER_ID)
      references USERS (USER_ID)
      on delete restrict on update restrict;

/*==============================================================*/
/* Index: SEQUENCE FOR ALL TABLE                                */
/*==============================================================*/

CREATE SEQUENCE users_user_id_seq;
ALTER TABLE users ALTER COLUMN user_id SET DEFAULT nextval('users_user_id_seq');
ALTER TABLE users ALTER COLUMN user_id SET NOT NULL;
ALTER SEQUENCE users_user_id_seq OWNED BY users.user_id;

CREATE SEQUENCE user_leave_request_user_leave_request_id_seq;
ALTER TABLE user_leave_request ALTER COLUMN user_leave_request_id SET DEFAULT nextval('user_leave_request_user_leave_request_id_seq');
ALTER TABLE user_leave_request ALTER COLUMN user_leave_request_id SET NOT NULL;
ALTER SEQUENCE user_leave_request_user_leave_request_id_seq OWNED BY user_leave_request.user_leave_request_id;

CREATE SEQUENCE bucket_approval_bucket_approval_id_seq;
ALTER TABLE bucket_approval ALTER COLUMN bucket_approval_id SET DEFAULT nextval('bucket_approval_bucket_approval_id_seq');
ALTER TABLE bucket_approval ALTER COLUMN bucket_approval_id SET NOT NULL;
ALTER SEQUENCE bucket_approval_bucket_approval_id_seq OWNED BY bucket_approval.bucket_approval_id;

CREATE SEQUENCE position_leave_position_leave_id_seq;
ALTER TABLE position_leave ALTER COLUMN position_leave_id SET DEFAULT nextval('position_leave_position_leave_id_seq');
ALTER TABLE position_leave ALTER COLUMN position_leave_id SET NOT NULL;
ALTER SEQUENCE position_leave_position_leave_id_seq OWNED BY position_leave.position_leave_id;

CREATE SEQUENCE positions_position_id_seq;
ALTER TABLE positions ALTER COLUMN position_id SET DEFAULT nextval('positions_position_id_seq');
ALTER TABLE positions ALTER COLUMN position_id SET NOT NULL;
ALTER SEQUENCE positions_position_id_seq OWNED BY positions.position_id;


